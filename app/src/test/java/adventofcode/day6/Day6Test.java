package adventofcode.day6;

import org.junit.Test;

import static org.junit.Assert.*;

public class Day6Test {
    @Test
    public void getSolution1() {
        assertEquals("7", new Day6().getSolution1());
        assertEquals("5", new Day6() {
            @Override
            public String getInputString() {
                return "bvwbjplbgvbhsrlpgdmjqwftvncz";
            }
        }.getSolution1());
        assertEquals("6", new Day6() {
            @Override
            public String getInputString() {
                return "nppdvjthqldpwncqszvftbrmjlhg";
            }
        }.getSolution1());
        assertEquals("10", new Day6() {
            @Override
            public String getInputString() {
                return "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
            }
        }.getSolution1());
        assertEquals("11", new Day6() {
            @Override
            public String getInputString() {
                return "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";
            }
        }.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("19", new Day6() {
            @Override
            public String getInputString() {
                return "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
            }
        }.getSolution2());
        assertEquals("23", new Day6() {
            @Override
            public String getInputString() {
                return "bvwbjplbgvbhsrlpgdmjqwftvncz";
            }
        }.getSolution2());
        assertEquals("23", new Day6() {
            @Override
            public String getInputString() {
                return "nppdvjthqldpwncqszvftbrmjlhg";
            }
        }.getSolution2());
        assertEquals("29", new Day6() {
            @Override
            public String getInputString() {
                return "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
            }
        }.getSolution2());
        assertEquals("26", new Day6() {
            @Override
            public String getInputString() {
                return "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";
            }
        }.getSolution2());
    }
}