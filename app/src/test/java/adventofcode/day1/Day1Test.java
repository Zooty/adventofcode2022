package adventofcode.day1;

import org.junit.Test;

import static org.junit.Assert.*;

public class Day1Test {

    @Test
    public void getSolution1() {
        assertEquals("24000", new Day1().getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("45000", new Day1().getSolution2());
    }
}
