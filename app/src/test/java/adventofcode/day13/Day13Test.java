package adventofcode.day13;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day13Test {
    private final IDay day = new Day13();

    @Test
    public void getSolution1() {
        assertEquals("13", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("140", day.getSolution2());
    }
}