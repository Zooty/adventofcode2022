package adventofcode.day15;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day15Test {

    @Test
    public void getSolution1() {
        Day15 day = new Day15();
        assertEquals("26", day.getSolution1(10));
    }

    @Test
    public void getSolution2() {
        Day15 day = new Day15();
        assertEquals("56000011", day.getSolution2(20));
    }
}