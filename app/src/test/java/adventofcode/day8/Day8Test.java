package adventofcode.day8;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day8Test {
    private final IDay day = new Day8();

    @Test
    public void getSolution1() {
        assertEquals("21", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("8", day.getSolution2());
    }
}