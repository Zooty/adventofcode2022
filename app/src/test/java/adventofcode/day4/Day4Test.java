package adventofcode.day4;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day4Test {
    private final IDay day = new Day4();

    @Test
    public void getSolution1() {
        assertEquals("2", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("4", day.getSolution2());
    }
}
