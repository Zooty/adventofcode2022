package adventofcode.day10;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day10Test {

    @Test
    public void getSolution1() {
        IDay day = new Day10();
        assertEquals("13140", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        IDay day = new Day10();
        assertEquals("""
                        ##..##..##..##..##..##..##..##..##..##..
                        ###...###...###...###...###...###...###.
                        ####....####....####....####....####....
                        #####.....#####.....#####.....#####.....
                        ######......######......######......####
                        #######.......#######.......#######.....""",
                day.getSolution2().trim());
    }
}