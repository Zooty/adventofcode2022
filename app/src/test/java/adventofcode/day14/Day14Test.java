package adventofcode.day14;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day14Test {


    @Test
    public void getSolution1() {
        IDay day = new Day14();
        assertEquals("24", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        IDay day = new Day14();
        assertEquals("93", day.getSolution2());
    }
}