package adventofcode.day9;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day9Test {


    @Test
    public void getSolution1() {
        IDay day = new Day9();
        assertEquals("13", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        IDay day = new Day9() {
            @Override
            public String getInputString() {
                return """
                        R 5
                        U 8
                        L 8
                        D 3
                        R 17
                        D 10
                        L 25
                        U 20
                        """;
            }
        };
        assertEquals("36", day.getSolution2());
    }
}