package adventofcode.day12;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day12Test {

    @Test
    public void getSolution1() {
        IDay day = new Day12();
        assertEquals("31", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        IDay day = new Day12();
        assertEquals("29", day.getSolution2());
    }
}