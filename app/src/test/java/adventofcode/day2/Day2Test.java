package adventofcode.day2;

import org.junit.Test;

import static org.junit.Assert.*;

public class Day2Test {

    private final Day2 day2 = new Day2();

    @Test
    public void testGetSolution1() {
        assertEquals("15", day2.getSolution1());
    }

    @Test
    public void testGetSolution2() {
        assertEquals("12", day2.getSolution2());
    }
}
