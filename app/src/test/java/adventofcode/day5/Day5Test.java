package adventofcode.day5;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day5Test {

    private final IDay day = new Day5();

    @Test
    public void getSolution1() {
        assertEquals("CMZ", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("MCD", day.getSolution2());
    }
}