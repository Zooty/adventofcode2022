package adventofcode.day7;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day7Test {
    private final IDay day = new Day7();

    @Test
    public void testGetSolution1() {
        assertEquals("95437", day.getSolution1());
    }

    @Test
    public void testGetSolution2() {
        assertEquals("24933642", day.getSolution2());
    }
}
