package adventofcode.day11;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day11Test {
    IDay day = new Day11();

    @Test
    public void getSolution1() {
        assertEquals("10605", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("2713310158", day.getSolution2());
    }
}