package adventofcode.day3;

import adventofcode.IDay;
import org.junit.Test;

import static org.junit.Assert.*;

public class Day3Test {

    private final IDay day = new Day3();

    @Test
    public void getSolution1() {
        assertEquals("157", day.getSolution1());
    }

    @Test
    public void getSolution2() {
        assertEquals("70", day.getSolution2());
    }
}
