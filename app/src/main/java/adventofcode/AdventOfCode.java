package adventofcode;

import adventofcode.day1.Day1;
import adventofcode.day10.Day10;
import adventofcode.day11.Day11;
import adventofcode.day12.Day12;
import adventofcode.day13.Day13;
import adventofcode.day14.Day14;
import adventofcode.day15.Day15;
import adventofcode.day2.Day2;
import adventofcode.day3.Day3;
import adventofcode.day4.Day4;
import adventofcode.day5.Day5;
import adventofcode.day6.Day6;
import adventofcode.day7.Day7;
import adventofcode.day8.Day8;
import adventofcode.day9.Day9;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class AdventOfCode {
    // TODO: find all days automatically
    private final static List<IDay> days = Arrays.asList(
            new Day1(),
            new Day2(),
            new Day3(),
            new Day4(),
            new Day5(),
            new Day6(),
            new Day7(),
            new Day8(),
            new Day9(),
            new Day10(),
            new Day11(),
            new Day12(),
            new Day13(),
            new Day14(),
            new Day15()
    );

    /**
     * Runs the program, calculates every day's both solutions and prints it to the standard output.
     * If arguments are present, only those days are calculated
     *
     * @param args A list of numbers. Only those days will be processed.
     */
    public static void main(String[] args) {
        Predicate<IDay> filter = day -> true;
        if(args.length != 0) {
            filter = day -> Arrays.stream(args).map(Integer::parseInt).toList().contains(day.getDay());
        }
        days.stream().filter(filter).forEach(day -> System.out.printf("Day %d solutions:%n    1: %s, 2: %s%n", day.getDay(), day.getSolution1(), day.getSolution2()));
    }
}
