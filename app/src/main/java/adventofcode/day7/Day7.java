package adventofcode.day7;

import adventofcode.IDay;
import adventofcode.day7.console.Console;
import adventofcode.day7.filesystem.Directory;
import adventofcode.day7.filesystem.Filesystem;
import lombok.Getter;

import java.util.Comparator;
import java.util.List;

public class Day7 implements IDay {
    @Getter
    private final int day = 7;

    private final Filesystem filesystem;

    public Day7() {
        String inputString = getInputString();
        Console console = new Console(List.of(inputString.split("\n")));
        filesystem = new Filesystem();
        filesystem.discoverByCommands(console.getCommands());
    }

    @Override
    public String getSolution1() {
        return String.valueOf(filesystem.getRootDir().getAllDirectoriesBelow()
                .stream()
                .filter(directory -> directory.getSize() <= 100000)
                .mapToLong(Directory::getSize)
                .sum());
    }

    @Override
    public String getSolution2() {
        long usedSize = filesystem.getRootDir().getSize();
        return String.valueOf(filesystem.getRootDir().getAllDirectoriesBelow()
                .stream()
                .filter(directory -> usedSize - directory.getSize() < 70000000 - 30000000)
                .min(Comparator.comparingLong(Directory::getSize))
                .orElseThrow().getSize());
    }
}
