package adventofcode.day7.console;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Console {
    @Getter
    private final List<Command> commands = new ArrayList<>();

    public Console(List<String> inputString) {
        for (int i = 0; i < inputString.size();) {
            Command command = new Command(inputString.get(i));
            int responseIterator = i + 1;
            while (responseIterator < inputString.size()
                    && !inputString.get(responseIterator).startsWith("$")) {
                command.getOutput().add(inputString.get(responseIterator++));
            }
            commands.add(command);
            i = responseIterator;
        }
    }
}
