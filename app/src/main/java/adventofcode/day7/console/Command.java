package adventofcode.day7.console;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Command {
    private final String command;
    private final List<String> output = new ArrayList<>();

    public Command(String command) {
        this.command = command.replace("$ ", "");
    }
}
