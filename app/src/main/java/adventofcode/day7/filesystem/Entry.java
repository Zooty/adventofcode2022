package adventofcode.day7.filesystem;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
public abstract class Entry {
    protected long size = -1;
    @Setter(AccessLevel.NONE)
    protected  EntryType type;
    protected String name;
    protected Directory parent;

    enum EntryType {
        Directory,
        File
    }
}
