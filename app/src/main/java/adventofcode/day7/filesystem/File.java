package adventofcode.day7.filesystem;

public class File extends Entry {
    public File(String name) {
        this.type = EntryType.File;
        this.name = name;
    }
}
