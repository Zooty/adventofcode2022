package adventofcode.day7.filesystem;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Directory extends Entry {
    @Getter
    private final List<Entry> content = new ArrayList<>();
    public Directory(String name, Directory parent) {
        this.name = name;
        this.type = EntryType.Directory;
        this.parent = parent;
    }

    @Override
    public long getSize() {
        if (this.size == -1) {
            this.size = content.stream().mapToLong(Entry::getSize).sum();
        }
        return this.size;
    }

    public List<Directory> getAllDirectoriesBelow() {
        List<Directory> directSubdirectories = content.stream()
                .filter(entry -> entry.type == EntryType.Directory)
                .map(entry -> ((Directory) entry)).toList();
        List<Directory> directories = new ArrayList<>(directSubdirectories);
        directSubdirectories.forEach(directory ->
                directories.addAll(directory.getAllDirectoriesBelow()));
        return directories;
    }
}
