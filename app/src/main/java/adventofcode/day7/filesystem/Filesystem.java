package adventofcode.day7.filesystem;

import adventofcode.day7.console.Command;
import lombok.Getter;

import java.util.List;

public class Filesystem {
    @Getter
    private final Directory rootDir;

    @Getter
    private Directory currentDirectory;
    public Filesystem() {
        rootDir = new Directory("/", null);
        currentDirectory = rootDir;
    }

    public void discoverByCommands(List<Command> commands) {
        commands.forEach(command -> {
            if (command.getCommand().startsWith("ls")) {
                handleLs(command);
            } else if (command.getCommand().startsWith("cd")) {
                handleCd(command);
            } else {
                throw new IllegalArgumentException("Unknown command " + command);
            }
        });
    }

    private void handleCd(Command command) {
        String cdTo = command.getCommand().replace("cd ", "");
        switch (cdTo.trim()) {
            case "/" -> currentDirectory = rootDir;
            case ".." -> currentDirectory = currentDirectory.getParent();
            default -> currentDirectory = (Directory) currentDirectory.getContent()
                    .stream()
                    .filter(entry -> entry.type == Entry.EntryType.Directory)
                    .filter(entry -> cdTo.contains(entry.getName()))
                    .findAny().orElseThrow();
        }
    }

    private void handleLs(Command command) {
        command.getOutput().forEach(output -> {
            if (output.startsWith("dir")) {
                currentDirectory.getContent().add(new Directory(output.replace("dir ", "").trim(), currentDirectory));
            } else {
                String[] split = output.split(" ");
                File newFile = new File(split[1].trim());
                newFile.setSize(Long.parseLong(split[0]));
                currentDirectory.getContent().add(newFile);
            }
        });
    }
}
