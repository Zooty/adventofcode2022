package adventofcode.day8;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Comparator;
import java.util.Optional;
import java.util.function.Predicate;

@Data
public class Tree {
    private final int positionX;
    private final int positionY;
    @EqualsAndHashCode.Exclude
    private final int height;


    public boolean isVisible(Forest forest) {
        return isVisibleFromOneSite(forest, this::lookUp)
                || isVisibleFromOneSite(forest, this::lookDown)
                || isVisibleFromOneSite(forest, this::lookLeft)
                || isVisibleFromOneSite(forest, this::lookRight);

    }
    private boolean isVisibleFromOneSite(Forest forest, Predicate<Tree> predicate) {
        return forest.getTrees().stream()
                .filter(predicate)
                .noneMatch(tree -> tree.getHeight() >= getHeight());
    }

    public int getScenicScore(Forest forest) {
        return getVisibleTreeCountFromOneSide(forest, this::lookUp) *
                getVisibleTreeCountFromOneSide(forest, this::lookDown) *
                getVisibleTreeCountFromOneSide(forest, this::lookRight) *
                getVisibleTreeCountFromOneSide(forest, this::lookLeft);
    }

    private int getVisibleTreeCountFromOneSide(Forest forest, Predicate<Tree> predicate) {
        Optional<Tree> closestHighTree = forest.getTrees().stream().filter(predicate)
                .filter(tree -> tree.getHeight() >= getHeight())
                .min(Comparator.comparingInt(o -> o.getDistance(this)));

        return closestHighTree.map(tree -> tree.getDistance(this))
                .orElseGet(() -> (int) forest.getTrees().stream().filter(predicate).count());
    }

    /**
     * Manhattan distance
     * @param tree other tree
     * @return the Manhattan distance
     */
    public int getDistance(Tree tree) {
        return Math.abs(tree.getPositionY() - getPositionY()) + Math.abs(tree.getPositionX() - getPositionX());
    }

    private boolean lookUp(Tree tree) {
        return tree.getPositionY() == getPositionY() && tree.getPositionX() > getPositionX();
    }

    private boolean lookDown(Tree tree) {
        return tree.getPositionY() == getPositionY() && tree.getPositionX() < getPositionX();
    }

    private boolean lookLeft(Tree tree) {
        return tree.getPositionX() == getPositionX() && tree.getPositionY() > getPositionY();
    }

    private boolean lookRight(Tree tree) {
        return tree.getPositionX() == getPositionX() && tree.getPositionY() < getPositionY();
    }
}
