package adventofcode.day8;

import adventofcode.IDay;
import lombok.Getter;

public class Day8 implements IDay {
    @Getter
    private final int day = 8;
    private final Forest forest;
    public Day8() {
        this.forest = new Forest(getInputString());
    }

    @Override
    public String getSolution1() {
        return String.valueOf(forest.getVisibleTrees().size());
    }

    @Override
    public String getSolution2() {
        return String.valueOf(forest.getHighestScenicScoreTree().getScenicScore(forest));
    }
}
