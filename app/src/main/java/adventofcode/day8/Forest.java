package adventofcode.day8;

import lombok.Getter;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;

public class Forest {
    @Getter
    private final Set<Tree> trees = new HashSet<>();
    public Forest(String inputString) {
        String[] lines = inputString.split("\n");
        for (int i = 0; i < lines.length; i++) {
            for (int j = 0; j < lines[i].trim().length(); j++) {
                trees.add(new Tree(i, j, Character.getNumericValue(lines[i].charAt(j))));
            }
        }
    }

    public Collection<Tree> getVisibleTrees() {
        return trees.stream().filter(tree -> tree.isVisible(this)).toList();
    }

    public Tree getHighestScenicScoreTree() {
        return trees.stream().max(Comparator.comparingInt(trees -> trees.getScenicScore(this))).orElseThrow();
    }
}
