package adventofcode.day5;

import lombok.Data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class Order {
    private final int moveAmount;
    private final int from;
    private final int to;

    public Order(String input) {
        Matcher matcher = Pattern.compile("(.*?)(?<moveAmount>\\d+)(.*?)(?<from>\\d+)(.*?)(?<to>\\d+)").matcher(input.trim());
        if (!matcher.find()) {
            throw new IllegalArgumentException("Formatting error in " + input);
        }
        moveAmount = Integer.parseInt(matcher.group("moveAmount"));
        from = Integer.parseInt(matcher.group("from"));
        to = Integer.parseInt(matcher.group("to"));
    }
}
