package adventofcode.day5;

import lombok.Data;

@Data
public class Crate {
    private final char name;
}
