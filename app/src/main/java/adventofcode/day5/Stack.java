package adventofcode.day5;

import lombok.Data;

@Data
public class Stack {
    private final java.util.Stack<Crate> crates = new java.util.Stack<>();

    public void push(Crate crate) {
        crates.push(crate);
    }

    public Crate pop() {
        return crates.pop();
    }

    public Crate peek() {
        return crates.peek();
    }
}
