package adventofcode.day5;

import adventofcode.IDay;
import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day5 implements IDay {
    @Getter
    private final int day = 5;
    private final List<Order> orders = new ArrayList<>();
    private final Map<Integer, Stack> stacks = new HashMap<>();

    @Override
    public String getSolution1() {
        init();
        orders.forEach(order -> {
            for (int i = 0; i < order.getMoveAmount(); i++) {
                pushCrate(order.getTo(), popCrate(order.getFrom()));
            }
        });

        return getResult();
    }

    @Override
    public String getSolution2() {
        init();
        orders.forEach(order -> {
            java.util.Stack<Crate> buffer = new java.util.Stack<>();
            for (int i = 0; i < order.getMoveAmount(); i++) {
                buffer.push(popCrate(order.getFrom()));
            }
            while (!buffer.isEmpty()) {
                pushCrate(order.getTo(), buffer.pop());
            }
        });

        return getResult();
    }

    private void init() {
        orders.clear();
        stacks.clear();
        parse();
    }

    private void parse() {
        List<String> lines = List.of(getInputString().split("\n"));
        int indexOfEmptyLine = IntStream.range(0, lines.size())
                .filter(value -> lines.get(value).trim().isEmpty())
                .findAny().orElseThrow();
        parseCrates(lines.stream().limit(indexOfEmptyLine - 1).collect(Collectors.toList()));
        parseOrders(lines.stream().skip(indexOfEmptyLine + 1).collect(Collectors.toList()));
    }

    private void parseOrders(List<String> ordersStrings) {
        ordersStrings.forEach(orderString -> orders.add(new Order(orderString)));
    }

    private void parseCrates(List<String> cratesStrings) {
        int numberOfStacks = (cratesStrings.get(cratesStrings.size() - 1).length() + 1) / 4;
        for (int i = 0; i < numberOfStacks; i++) {
            stacks.put(i, new Stack());
        }

        for (int indexOfLine = cratesStrings.size() - 1; indexOfLine >= 0; indexOfLine--) {
            for (int indexOfCrate = 0; indexOfCrate < numberOfStacks; indexOfCrate++) {
                if (cratesStrings.get(indexOfLine).length() > indexOfCrate * 4 + 1
                        && cratesStrings.get(indexOfLine).charAt(indexOfCrate * 4 + 1) != ' ') {
                    stacks.get(indexOfCrate).getCrates().push(new Crate(cratesStrings.get(indexOfLine).charAt(indexOfCrate * 4 + 1)));
                }
            }
        }
    }

    private Crate popCrate(int number) {
        return stacks.get(number - 1).pop();
    }

    private void pushCrate(int number, Crate crate) {
        stacks.get(number - 1).push(crate);
    }

    private String getResult() {
        return stacks.values().stream()
                .map(stack -> stack.peek().getName())
                .map(Object::toString)
                .collect(Collectors.joining());
    }
}
