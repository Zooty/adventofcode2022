package adventofcode.day12;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.javatuples.Pair;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Square {
    @EqualsAndHashCode.Include
    private final Pair<Integer, Integer> coordinate;
    private final int height;
    private final char heightChar;
    @NonNull
    private Boolean start;
    private final boolean end;
    private int stepsNeeded = -1;
    public boolean isVisited() {
        return stepsNeeded != -1;
    }
}
