package adventofcode.day12;

import adventofcode.IDay;
import lombok.Getter;

public class Day12 implements IDay {
    @Getter
    private final int day = 12;

    @Override
    public String getSolution1() {
        Map map = new Map(getInputString());
        map.startWalking();
        return String.valueOf(map.getMinimumSteps());
    }

    @Override
    public String getSolution2() {
        Map map = new Map(getInputString());
        map.setStartingSquares('a');
        map.startWalking();
        return String.valueOf(map.getMinimumSteps());
    }
}
