package adventofcode.day12;

import lombok.Getter;
import org.javatuples.Pair;

import java.util.*;
import java.util.List;

public class Map {
    private Integer minimumSteps = null;
    private final HashMap<Pair<Integer, Integer>, Square> squareSet = new HashMap<>();
    @Getter
    private boolean isEndReached = false;

    public Map(String input) {
        String[] lines = input.split("\n");
        for (int y = 0; y < lines.length; y++) {
            for (int x = 0; x < lines[y].length(); x++) {
                char character = lines[y].charAt(x);
                Pair<Integer, Integer> coordinate = new Pair<>(x, lines.length - 1 - y);
                switch (character) {
                    case 'S' -> squareSet.put(coordinate, new Square(coordinate, 'a', character, true, false));
                    case 'E' -> squareSet.put(coordinate, new Square(coordinate, 'E', character, false, true));
                    default -> squareSet.put(coordinate, new Square(coordinate, character, character, false, false));
                }
            }
        }
    }

    public void startWalking() {
        squareSet.values().stream().filter(Square::getStart).forEach(square -> {
            startWalking(square);
            if (isEndReached) {
                int currentMinimumSteps = calculateMinimumSteps();
                if (minimumSteps == null || minimumSteps > currentMinimumSteps) {
                    minimumSteps = currentMinimumSteps;
                }
            }
            resetWalks();
        });
    }

    private void resetWalks() {
        squareSet.values().forEach(square -> square.setStepsNeeded(-1));
        isEndReached = false;
    }


    public void startWalking(Square start) {
        Square end = squareSet.values().stream().filter(Square::isEnd).findAny().orElseThrow();
        Queue<Square> visitingNodes = new LinkedList<>();
        visitingNodes.add(start);
        start.setStepsNeeded(0);
        while (!visitingNodes.contains(end)) {
            if (visitingNodes.isEmpty()) {
                isEndReached = false;
                return;
            }
            Square stepOn = visitingNodes.poll();
            Collection<Square> unvisitedNeighbours = getUnvisitedNeighbours(stepOn);
            unvisitedNeighbours.forEach(square -> square.setStepsNeeded(stepOn.getStepsNeeded() + 1));
            visitingNodes.addAll(unvisitedNeighbours);
        }
        isEndReached = true;
    }

    private Collection<Square> getUnvisitedNeighbours(Square square) {
        List<Square> neighbours = new ArrayList<>();
        if (canStep(square, getLeft(square))) neighbours.add(getLeft(square));
        if (canStep(square, getDown(square))) neighbours.add(getDown(square));
        if (canStep(square, getUp(square))) neighbours.add(getUp(square));
        if (canStep(square, getRight(square))) neighbours.add(getRight(square));
        return neighbours;
    }


    private boolean canStep(Square current, Square to) {
        if (to == null) return false;
        if (to.isVisited()) return false;
        return current.getHeight() + 1 >= to.getHeight();
    }

    private Square getLeft(Square current) {
        return squareSet.getOrDefault(new Pair<>(current.getCoordinate().getValue0() - 1, current.getCoordinate().getValue1()), null);
    }

    private Square getRight(Square current) {
        return squareSet.getOrDefault(new Pair<>(current.getCoordinate().getValue0() + 1, current.getCoordinate().getValue1()), null);
    }

    private Square getDown(Square current) {
        return squareSet.getOrDefault(new Pair<>(current.getCoordinate().getValue0(), current.getCoordinate().getValue1() - 1), null);
    }

    private Square getUp(Square current) {
        return squareSet.getOrDefault(new Pair<>(current.getCoordinate().getValue0(), current.getCoordinate().getValue1() + 1), null);
    }

    public int getMinimumSteps() {
        if (minimumSteps == null) {
            throw new IllegalStateException("The endpoint was never visited");
        }
        return minimumSteps;
    }

    private int calculateMinimumSteps() {
        Square end = squareSet.values().parallelStream().filter(Square::isEnd).findAny().orElseThrow();
        if (!isEndReached || !end.isVisited()) throw new IllegalStateException("The endpoint was never visited");
        return end.getStepsNeeded();
    }

    public void setStartingSquares(char character) {
        squareSet.values().parallelStream().filter(square -> square.getHeightChar() == character).forEach(square -> square.setStart(true));
    }
}

