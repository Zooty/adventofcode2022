package adventofcode.day3;

import adventofcode.IDay;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Day3 implements IDay {

    @Getter
    private final int day = 3;

    private final List<Group> groups = new ArrayList<>();

    public Day3() {
        String[] lines = getInputString().split("\n");
        for (int i = 0; i < lines.length; i+=3) {
            groups.add(new Group(List.of(lines[i], lines[i+1], lines[i+2])));
        }
    }

    @Override
    public String getSolution1() {
        return String.valueOf(groups.stream().flatMap(group -> group.getRucksacks().stream()).mapToInt(rucksack -> rucksack.getRedundantItem().getPriority()).sum());
    }

    @Override
    public String getSolution2() {
        return String.valueOf(groups.stream().mapToInt(Group::getBadgePriority).sum());
    }
}
