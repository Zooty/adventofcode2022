package adventofcode.day3;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Rucksack {
    @Getter
    private final List<Item> leftCompartment = new ArrayList<>();
    @Getter
    private final List<Item> rightCompartment = new ArrayList<>();

    public Rucksack(String input) {
        for (int i = 0; i < input.length(); i++) {
            if (i < input.length() / 2) {
                leftCompartment.add(new Item(input.charAt(i)));
            } else {
                rightCompartment.add(new Item(input.charAt(i)));
            }
        }
    }

    public Item getRedundantItem() {
        for (Item itemL : leftCompartment) {
            for (Item itemR : rightCompartment) {
                if (itemL.equals(itemR))
                    return itemL;
            }
        }
        return null;
    }

    public Stream<Item> getAllItemStream() {
        return Stream.concat(leftCompartment.stream(), rightCompartment.stream());
    }
}
