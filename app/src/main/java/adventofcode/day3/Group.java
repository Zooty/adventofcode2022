package adventofcode.day3;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Group {
    @Getter
    private final List<Rucksack> rucksacks = new ArrayList<>(3);

    public Group(List<String> inputs) {
        for (String input : inputs) {
            rucksacks.add(new Rucksack(input));
        }
    }

    public int getBadgePriority() {
        return rucksacks.get(0).getAllItemStream().filter(rucksacks.get(1).getAllItemStream().collect(Collectors.toList())::contains).filter(rucksacks.get(2).getAllItemStream().collect(Collectors.toList())::contains).findAny().orElseThrow().getPriority();
    }
}
