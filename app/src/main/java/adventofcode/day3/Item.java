package adventofcode.day3;

import lombok.Data;


@Data
public class Item {
    private final Character itemChar;
    public int getPriority() {
        return Character.isLowerCase(this.itemChar) ?
                ((int) this.itemChar) - 97 + 1:
                ((int) this.itemChar) - 65 + 27;
    }
}
