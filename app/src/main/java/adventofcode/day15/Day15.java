package adventofcode.day15;

import adventofcode.IDay;
import lombok.Getter;

public class Day15 implements IDay {
    @Getter
    private final int day = 15;
    private final TunnelZone tunnelZone;

    public Day15() {
        tunnelZone = new TunnelZone(getInputString());
    }

    public String getSolution1(int line) {
        return String.valueOf(tunnelZone.getCoveredRowsInLine(line));
    }

    public String getSolution2(int maxLine) {
        return String.valueOf(tunnelZone.findBeaconTuningFrequency(maxLine));
    }

    @Override
    public String getSolution1() {
        return getSolution1(2000000);
    }

    @Override
    public String getSolution2() {
        return getSolution2(4000000);
    }
}
