package adventofcode.day15;

import lombok.Data;
import org.javatuples.Pair;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class Sensor {
    private final Pair<Integer, Integer> position;
    private final Pair<Integer, Integer> closestBeacon;

    Sensor(String input) {
        Matcher matcher = Pattern.compile("(^.*?x=)(?<posX>-?\\d+)(,\\sy=)(?<posY>-?\\d+)(.*?x=)(?<beaconX>-?\\d+)(, y=)(?<beaconY>-?\\d+)").matcher(input);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Formatting error in " + input);
        }
        position = new Pair<>(Integer.parseInt(matcher.group("posX")), Integer.parseInt(matcher.group("posY")));
        closestBeacon = new Pair<>(Integer.parseInt(matcher.group("beaconX")), Integer.parseInt(matcher.group("beaconY")));
    }

    public int getRange() {
        return Math.abs(position.getValue0() - closestBeacon.getValue0()) + Math.abs(position.getValue1() - closestBeacon.getValue1());
    }
}
