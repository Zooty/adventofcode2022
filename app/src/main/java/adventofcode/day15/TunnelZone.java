package adventofcode.day15;

import org.javatuples.Pair;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class TunnelZone {
    private final List<Sensor> sensors = new ArrayList<>();

    public TunnelZone(String input) {
        String[] lines = input.split("\n");
        for (String line : lines) {
            sensors.add(new Sensor(line));
        }
    }

    public int getCoveredRowsInLine(int line) {
        List<Pair<Integer, Integer>> coverRanges = getCoverRanges(line);
        int minimumColumn = coverRanges.stream().mapToInt(Pair::getValue0).min().orElseThrow();
        int maximumColumn = coverRanges.stream().mapToInt(Pair::getValue1).max().orElseThrow();
        int sumOfCoveredPlaces = 0;
        for (int i = minimumColumn; i <= maximumColumn; i++) {
            if (anyRangeContains(i, coverRanges)) {
                sumOfCoveredPlaces++;
            }
        }
        return sumOfCoveredPlaces - beaconsInLine(line, minimumColumn, maximumColumn);
    }

    @NotNull
    private List<Pair<Integer, Integer>> getCoverRanges(int line) {
        return sensors.stream()
                .map(sensor -> getCoversOnLine(line, sensor)).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private int beaconsInLine(int line, int minimumColumn, int maximumColumn) {
        Set<Pair<Integer, Integer>> beacons = sensors.stream().map(Sensor::getClosestBeacon).collect(Collectors.toSet());
        return (int) beacons.stream().filter(beacon -> beacon.getValue1() == line
                        && minimumColumn <= beacon.getValue0()
                        && beacon.getValue0() <= maximumColumn)
                .count();
    }

    private boolean anyRangeContains(int place, List<Pair<Integer, Integer>> coverRanges) {
        for (Pair<Integer, Integer> range : coverRanges) {
            if (range.getValue0() <= place && place <= range.getValue1()) {
                return true;
            }
        }
        return false;
    }

    private Pair<Integer, Integer> getCoversOnLine(int line, Sensor sensor) {
        Pair<Integer, Integer> position = sensor.getPosition();
        int range = sensor.getRange();
        int rowDistance = Math.abs(position.getValue1() - line);
        if (rowDistance > range) {
            return null;
        }
        return new Pair<>(position.getValue0() - (range - rowDistance), position.getValue0() + (range - rowDistance));
    }

    public Long findBeaconTuningFrequency(int maxLine) {
        for (int i = maxLine - 1; i >= 0; i--) {
            List<Pair<Integer, Integer>> coverRanges = getCoverRanges(i);
            for (int j = 0; j < maxLine; j++) {
                int finalJ = j;
                Optional<Pair<Integer, Integer>> foundRange = coverRanges.parallelStream().filter(range -> range.getValue0() <= finalJ && finalJ <= range.getValue1()).findAny();
                if (foundRange.isPresent()) {
                    j = foundRange.get().getValue1();
                } else {
                    return (long) j * 4000000 + i;
                }
            }
        }

        return null;
    }
}
