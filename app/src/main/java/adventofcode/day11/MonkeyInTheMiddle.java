package adventofcode.day11;

import org.javatuples.Pair;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class MonkeyInTheMiddle {
    private final List<Monkey> monkeys = new ArrayList<>();

    public MonkeyInTheMiddle(String input) {
        for (String monkeyString : input.split("\n\n")) {
            monkeys.add(new Monkey(monkeyString));
        }
    }

    public void calculateRounds(int rounds, boolean haveRelief) {
        for (int i = 0; i < rounds; i++) {
            monkeys.forEach(monkey -> {
                monkey.inspectItems();
                if (haveRelief) {
                    monkey.bore();
                }
                List<Pair<Integer, BigInteger>> theYeets = monkey.yeetAll();
                for (Pair<Integer, BigInteger> theYeet : theYeets) {
                    monkeys.stream()
                            .filter(yoinker -> yoinker.getId() == theYeet.getValue0())
                            .findAny().orElseThrow()
                            .yoink(theYeet.getValue1());
                }
            });
            int cheatCode = monkeys.stream()
                    .mapToInt(Monkey::getTestDivider)
                    .reduce(1, (left, right) -> left * right);
            monkeys.forEach(monkey -> monkey.cheatBore(cheatCode));
        }
    }

    public void calculateRounds(int rounds) {
        calculateRounds(rounds, true);
    }

    public String mostActiveValue() {
        return String.valueOf(monkeys.stream()
                .mapToLong(Monkey::getItemsInspected)
                .sorted()
                .skip(monkeys.size() - 2)
                .reduce(1, (left, right) -> left * right));
    }
}
