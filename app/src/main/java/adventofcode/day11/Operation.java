package adventofcode.day11;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.math.BigInteger;

@Data
public class Operation {
    /**
     * -1 means OLD
     */
    private final int firstOperand;
    /**
     * -1 means OLD
     */
    private final int secondOperand;
    private final Operator operator;

    public Operation(String input) {
        String[] split = input.trim().split(" ");
        firstOperand = parseOperand(split[0]);
        secondOperand = parseOperand(split[2]);
        switch (split[1]) {
            case "+" -> operator = Operator.Addition;
            case "*" -> operator = Operator.Multiply;
            default -> throw new IllegalStateException("Unexpected value: " + split[1]);
        }
    }

    public BigInteger apply(BigInteger old) {
        BigInteger secondOperand = this.secondOperand == -1 ? old : BigInteger.valueOf(this.secondOperand);
        return switch (operator) {
            case Multiply -> old.multiply(secondOperand);
            case Addition -> old.add(secondOperand);
        };
    }

    private int parseOperand(String input) {
        if ("old".equals(input)) {
            return -1;
        }
        return Integer.parseInt(input);
    }

    @AllArgsConstructor
    private enum Operator {
        Multiply("*"),
        Addition("+");
        @Getter
        private final String stringRepresentation;
    }
}