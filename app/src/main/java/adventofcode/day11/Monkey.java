package adventofcode.day11;

import lombok.Getter;
import org.javatuples.Pair;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Monkey {
    @Getter
    private final int id;
    @Getter
    private List<BigInteger> itemWorryLevels = new ArrayList<>();
    @Getter
    private final int testDivider;
    private final Operation operation;
    private final int trueThrowMoney;
    private final int falseThrowMonkey;
    @Getter
    private int itemsInspected = 0;

    public Monkey(String input) {
        String[] lines = input.split("\n");
        this.id = Character.getNumericValue(lines[0].charAt(7));
        this.itemWorryLevels.addAll(Arrays.stream(lines[1].replace("  Starting items: ", "").split(", "))
                .map(String::trim)
                .map(BigInteger::new)
                .toList());
        this.operation = new Operation(lines[2].replace("  Operation: new = ", "").trim());
        this.testDivider = Integer.parseInt(lines[3].replace("  Test: divisible by ", "").trim());
        this.trueThrowMoney = Integer.parseInt(lines[4].replace("    If true: throw to monkey ", "").trim());
        this.falseThrowMonkey = Integer.parseInt(lines[5].replace("    If false: throw to monkey ", "").trim());
    }

    public void inspectItems() {
        itemWorryLevels = itemWorryLevels.stream()
                .map(this.operation::apply)
                .collect(Collectors.toList());
        itemsInspected += itemWorryLevels.size();
    }

    public void bore() {
        itemWorryLevels = itemWorryLevels.parallelStream()
                .map(aLong -> aLong.divide(BigInteger.valueOf(3)))
                .collect(Collectors.toList());
    }

    public void yoink(BigInteger itemWorryLevel) {
        itemWorryLevels.add(itemWorryLevel);
    }

    public List<Pair<Integer, BigInteger>> yeetAll() {
        List<Pair<Integer, BigInteger>> theYeets = itemWorryLevels.stream()
                .map(aLong -> new Pair<>(whoToThrow(aLong), aLong))
                .toList();
        itemWorryLevels.clear();
        return theYeets;
    }

    private int whoToThrow(BigInteger itemWorry) {
        return itemWorry.remainder(BigInteger.valueOf(testDivider)).equals(BigInteger.ZERO) ? trueThrowMoney : falseThrowMonkey;
    }

    public void cheatBore(int cheatCode) {
        itemWorryLevels = getItemWorryLevels().stream()
                .map(bigInteger -> bigInteger.remainder(BigInteger.valueOf(cheatCode)))
                .collect(Collectors.toList());
    }
}