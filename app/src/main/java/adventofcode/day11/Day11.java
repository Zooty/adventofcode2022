package adventofcode.day11;

import adventofcode.IDay;
import lombok.Getter;

public class Day11 implements IDay {
    @Getter
    private final int day = 11;


    @Override
    public String getSolution1() {
        MonkeyInTheMiddle monkeyInTheMiddle = new MonkeyInTheMiddle(getInputString());
        monkeyInTheMiddle.calculateRounds(20);
        return monkeyInTheMiddle.mostActiveValue();
    }

    @Override
    public String getSolution2() {
        MonkeyInTheMiddle monkeyInTheMiddle = new MonkeyInTheMiddle(getInputString());
        monkeyInTheMiddle.calculateRounds(10000, false);
        return monkeyInTheMiddle.mostActiveValue();
    }
}
