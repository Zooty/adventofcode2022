package adventofcode.day14;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.javatuples.Pair;

@EqualsAndHashCode(callSuper = true)
public class Sand extends Unit {
    @Getter
    @Setter
    private boolean isInRest = false;

    public Sand(Pair<Integer, Integer> position) {
        super(position);
    }
}
