package adventofcode.day14;

import adventofcode.IDay;
import lombok.Getter;

public class Day14 implements IDay {
    @Getter
    final int day = 14;

    @Override
    public String getSolution1() {
        Cave cave = new Cave(getInputString());
        cave.simulate(false);
        return String.valueOf(cave.getAmountOfSand());
    }

    @Override
    public String getSolution2() {
        Cave cave = new Cave(getInputString());
        cave.simulate(true);
        return String.valueOf(cave.getAmountOfSand());
    }
}
