package adventofcode.day14;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.javatuples.Pair;

@Data
@AllArgsConstructor
public abstract class Unit {
    private Pair<Integer, Integer> position;
}
