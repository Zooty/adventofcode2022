package adventofcode.day14;

import lombok.EqualsAndHashCode;
import org.javatuples.Pair;

@EqualsAndHashCode(callSuper = true)
public class Rock extends Unit {
    public Rock(Pair<Integer, Integer> position) {
        super(position);
    }
}
