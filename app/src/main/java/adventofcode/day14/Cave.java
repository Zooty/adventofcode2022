package adventofcode.day14;


import org.javatuples.Pair;

import java.util.HashMap;

public class Cave {
    private final Pair<Integer, Integer> sandStartingPoint = new Pair<>(500, 0);
    private final HashMap<Pair<Integer, Integer>, Unit> units = new HashMap<>();

    public Cave(String input) {
        String[] lines = input.split("\n");
        for (String line : lines) {
            String[] coordinates = line.split(" -> ");
            String currentCoordinate = coordinates[0];
            for (int i = 1; i < coordinates.length; i++) {
                addRocks(currentCoordinate.split(","), coordinates[i].split(","));
                currentCoordinate = coordinates[i];
            }
        }
    }

    public void simulate(boolean hasFloor) {
        Sand fallingSand = createNewSand();
        int abyssHeight = units.values().stream().mapToInt(unit -> unit.getPosition().getValue1()).max().orElseThrow();
        if (hasFloor) {
            addFloor(abyssHeight);
            abyssHeight += 2;
        }

        while (true) {
            if (fallingSand.isInRest()) {
                if (fallingSand.getPosition().equals(sandStartingPoint)) {
                    break;
                } else {
                    fallingSand = createNewSand();
                }
            }
            if (fallingSand.getPosition().getValue1() > abyssHeight) {
                break;
            }

            simulateFalling(fallingSand);
        }
    }

    public long getAmountOfSand() {
        return units.values().stream().filter(unit -> unit instanceof Sand && ((Sand) unit).isInRest()).count();
    }

    private void addFloor(int abyssHeight) {
        for (int i = 0; i < 1000; i++) {
            Pair<Integer, Integer> coordinate = new Pair<>(i, abyssHeight + 2);
            units.put(coordinate, new Rock(coordinate));
        }
    }

    private void addRocks(String[] currentSplitCoordinates, String[] nextSplitCoordinates) {
        if (currentSplitCoordinates[0].equals(nextSplitCoordinates[0])) {
            int right = Integer.parseInt(currentSplitCoordinates[0]);
            for (int height = Math.min(Integer.parseInt(currentSplitCoordinates[1]), Integer.parseInt(nextSplitCoordinates[1]));
                 height <= Math.max(Integer.parseInt(currentSplitCoordinates[1]), Integer.parseInt(nextSplitCoordinates[1])); height++) {
                Pair<Integer, Integer> coordinate = new Pair<>(right, height);
                units.put(coordinate, new Rock(coordinate));
            }
        } else {
            int height = Integer.parseInt(currentSplitCoordinates[1]);
            for (int right = Math.min(Integer.parseInt(currentSplitCoordinates[0]), Integer.parseInt(nextSplitCoordinates[0]));
                 right <= Math.max(Integer.parseInt(currentSplitCoordinates[0]), Integer.parseInt(nextSplitCoordinates[0])); right++) {
                Pair<Integer, Integer> coordinate = new Pair<>(right, height);
                units.put(coordinate, new Rock(coordinate));
            }
        }
    }

    private void moveUnit(Unit unit, Pair<Integer, Integer> newPosition) {
        units.remove(unit.getPosition());
        unit.setPosition(newPosition);
        units.put(unit.getPosition(), unit);
    }

    private Sand createNewSand() {
        Sand newSand = new Sand(sandStartingPoint);
        units.put(newSand.getPosition(), newSand);
        return newSand;
    }

    private void simulateFalling(Sand fallingSand) {
        if (!units.containsKey(getBelow(fallingSand.getPosition()))) {
            moveUnit(fallingSand, getBelow(fallingSand.getPosition()));
            return;
        }
        if (!units.containsKey(getBelowLeft(fallingSand.getPosition()))) {
            moveUnit(fallingSand, getBelowLeft(fallingSand.getPosition()));
            return;
        }
        if (!units.containsKey(getBelowRight(fallingSand.getPosition()))) {
            moveUnit(fallingSand, getBelowRight(fallingSand.getPosition()));
            return;
        }
        fallingSand.setInRest(true);
    }

    private Pair<Integer, Integer> getBelow(Pair<Integer, Integer> position) {
        return new Pair<>(position.getValue0(), position.getValue1() + 1);
    }

    private Pair<Integer, Integer> getBelowLeft(Pair<Integer, Integer> position) {
        return new Pair<>(position.getValue0() - 1, position.getValue1() + 1);
    }

    private Pair<Integer, Integer> getBelowRight(Pair<Integer, Integer> position) {
        return new Pair<>(position.getValue0() + 1, position.getValue1() + 1);
    }
}
