package adventofcode.day4;

import lombok.Data;

@Data
public class Pair {
    private final Section sectionA;
    private final Section sectionB;

    public Pair(String input) {
        String[] split = input.trim().split(",");
        sectionA = new Section(split[0]);
        sectionB = new Section(split[1]);
    }

    public boolean doesContainsEachOther() {
        return doesAContainsB() || doesBContainsA();
    }

    public boolean doesOverlap() {
        return sectionA.getBeginning() >= sectionB.getBeginning() && sectionA.getBeginning() <= sectionB.getEnd()
                || sectionA.getEnd() >= sectionB.getBeginning() && sectionA.getEnd() <= sectionB.getEnd()
                || sectionB.getBeginning() >= sectionA.getBeginning() && sectionB.getEnd() <= sectionA.getEnd();
    }

    private boolean doesAContainsB() {
        return sectionA.getBeginning() <= sectionB.getBeginning() && sectionA.getEnd() >= sectionB.getEnd();
    }

    private boolean doesBContainsA() {
        return sectionB.getBeginning() <= sectionA.getBeginning() && sectionB.getEnd() >= sectionA.getEnd();
    }
}
