package adventofcode.day4;

import adventofcode.IDay;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Day4 implements IDay {
    @Getter
    private final int day = 4;

    private final List<Pair> pairs = new ArrayList<>();

    public Day4() {
        Arrays.stream(getInputString().trim().split("\n")).forEach(line -> pairs.add(new Pair(line)));
    }

    @Override
    public String getSolution1() {
        return String.valueOf(pairs.stream().filter(Pair::doesContainsEachOther).count());
    }

    @Override
    public String getSolution2() {
        return String.valueOf(pairs.stream().filter(Pair::doesOverlap).count());
    }
}
