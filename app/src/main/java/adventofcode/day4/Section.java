package adventofcode.day4;

import lombok.Data;

@Data
public class Section {
    private final int beginning;
    private final int end;

    public Section(String input) {
        String[] split = input.split("-");
        beginning = Integer.parseInt(split[0]);
        end = Integer.parseInt(split[1]);
    }
}
