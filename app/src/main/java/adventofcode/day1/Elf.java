package adventofcode.day1;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Elf {
    @Getter
    private final List<Integer> calories = new ArrayList<>();

    public int sumOfCalories() {
        return calories.stream().reduce(0, Integer::sum);
    }

    public void addFood(int calorie) {
        calories.add(calorie);
    }
}
