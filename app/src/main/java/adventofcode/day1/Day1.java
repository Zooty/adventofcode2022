package adventofcode.day1;

import adventofcode.IDay;
import lombok.Getter;

import java.util.ArrayList;

public class Day1 implements IDay {
    private final Convoy convoy = new Convoy();

    public Day1() {
        String input = getInputString();
        convoy.parse(input);
    }

    @Getter
    private final int day = 1;

    @Override
    public String getSolution1() {
        Elf elfWithTheBiggestDick = convoy.getMostLoadedElf();
        return "" + elfWithTheBiggestDick.sumOfCalories();
    }

    @Override
    public String getSolution2() {
        ArrayList<Elf> topSumCalories = new ArrayList<>();
        Elf mostLoadedElf = convoy.getMostLoadedElf();
        topSumCalories.add(mostLoadedElf);
        convoy.getElves().remove(mostLoadedElf);
        mostLoadedElf = convoy.getMostLoadedElf();
        topSumCalories.add(mostLoadedElf);
        convoy.getElves().remove(mostLoadedElf);
        mostLoadedElf = convoy.getMostLoadedElf();
        topSumCalories.add(mostLoadedElf);
        return "" + topSumCalories.stream().map(Elf::sumOfCalories).reduce(0, Integer::sum);
    }
}
