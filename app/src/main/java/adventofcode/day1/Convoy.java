package adventofcode.day1;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Convoy {
    @Getter
    private final List<Elf> elves = new ArrayList<>();

    public Elf getMostLoadedElf() {
        return elves.stream().max(Comparator.comparingInt(Elf::sumOfCalories)).orElse(null);
    }

    public void parse(String input) {
        elves.clear();
        String[] allCalories = input.split("\n");
        Elf currentElf = new Elf();
        for (String calorieString : allCalories) {
            if (calorieString.trim().isEmpty()) {
                elves.add(currentElf);
                currentElf = new Elf();
            } else {
                currentElf.addFood(Integer.parseInt(calorieString.trim()));
            }
        }
        elves.add(currentElf);
    }
}
