package adventofcode.day2;

import adventofcode.IDay;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Day2 implements IDay {
    @Getter
    private final int day = 2;
    private final List<Game> games = new ArrayList<>();

    public Day2() {
        String inputString = getInputString();
        parse(inputString);
    }

    @Override
    public String getSolution1() {
        return String.valueOf(games.stream().mapToInt(Game::getScore).sum());
    }

    @Override
    public String getSolution2() {
        return String.valueOf(games.stream().mapToInt(Game::getCorrectScore).sum());
    }

    private void parse(String input) {
        for (String split : input.split("\n")) {
            games.add(new Game(split));
        }
    }

    private static class Game {

        @Getter
        final OpponentMove opponentMove;
        @Getter
        final MyMove myMove;

        public Game(String input) {
            String[] split = input.split(" ");
            switch (split[0].trim()) {
                case "A" -> opponentMove = OpponentMove.Rock;
                case "B" -> opponentMove = OpponentMove.Paper;
                case "C" -> opponentMove = OpponentMove.Scissor;
                default -> throw new IllegalArgumentException("Can't parse " + split[0]);
            }

            switch (split[1].trim()) {
                case "X" -> myMove = MyMove.X;
                case "Y" -> myMove = MyMove.Y;
                case "Z" -> myMove = MyMove.Z;
                default -> throw new IllegalArgumentException("Can't parse " + split[1]);
            }
        }

        public int getScore() {
            int score = 0;
            score += getGameResult();
            score += myMove.getScore();
            return score;
        }

        public int getCorrectScore() {
            return myMove.moveShouldScore(getOpponentMove()) + myMove.getGameShouldScore();
        }

        private int getGameResult() {
            if (opponentMove.getCanBeat() == myMove)
                return 0; // Lost
            if (opponentMove.getBeatenBy() == myMove)
                return 6; // Won
            return 3; // Draw
        }

        public enum OpponentMove {
            Rock(MyMove.Z, MyMove.Y), // Rock
            Paper(MyMove.X, MyMove.Z), // Paper
            Scissor(MyMove.Y, MyMove.X); // Scissor

            OpponentMove(MyMove canBeat, MyMove beatenBy) {
                this.canBeat = canBeat;
                this.beatenBy = beatenBy;
            }

            @Getter
            private final MyMove canBeat;

            @Getter
            private final MyMove beatenBy;
        }

        public enum MyMove {
            X(1, 0), // Rock, Lose
            Y(2, 3), // Paper, Draw
            Z(3, 6); // Scissor, Win

            @Getter
            private final int score;

            @Getter
            private final int gameShouldScore;

            MyMove(int score, int gameShouldScore) {
                this.score = score;
                this.gameShouldScore = gameShouldScore;
            }

            public int moveShouldScore(OpponentMove opponentMove) {
                switch (this) {
                    case X -> { // lose
                        switch (opponentMove) {
                            case Rock -> {
                                return Z.getScore();
                            }
                            case Paper -> {
                                return X.getScore();
                            }
                            case Scissor -> {
                                return Y.getScore();
                            }
                        }
                    }
                    case Y -> { // Draw
                        switch (opponentMove) {
                            case Rock -> {
                                return X.getScore();
                            }
                            case Paper -> {
                                return Y.getScore();
                            }
                            case Scissor -> {
                                return Z.getScore();
                            }
                        }
                    }
                    case Z -> { // Win
                        switch (opponentMove) {
                            case Rock -> {
                                return Y.getScore();
                            }
                            case Paper -> {
                                return Z.getScore();
                            }
                            case Scissor -> {
                                return X.getScore();
                            }
                        }
                    }
                }
                throw new IllegalArgumentException("This can't happen");
            }
        }
    }
}
