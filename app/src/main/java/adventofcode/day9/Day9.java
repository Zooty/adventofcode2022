package adventofcode.day9;

import adventofcode.IDay;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class Day9 implements IDay {
    @Getter
    private final int day = 9;

    private final List<Movement> movements = new ArrayList<>();

    public Day9() {
        for (String line : getInputString().split("\n")) {
            movements.add(new Movement(line.trim()));
        }
    }

    @Override
    public String getSolution1() {
        Field field = new Field(2);
        movements.forEach(field::move);
        return String.valueOf(field.getTailLocations().size());
    }

    @Override
    public String getSolution2() {
        Field field = new Field(10);
        movements.forEach(field::move);
        return String.valueOf(field.getTailLocations().size());
    }
}
