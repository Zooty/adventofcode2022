package adventofcode.day9;

import com.google.common.collect.Sets;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import org.javatuples.Pair;

import java.util.Collections;
import java.util.Set;

@Data
public class Knot {
    @Setter(AccessLevel.NONE)
    private int positionX = 0;
    @Setter(AccessLevel.NONE)
    private int positionY = 0;
    private final Set<Pair<Integer, Integer>> visitedLocations = Sets.newHashSet(Collections.singleton(new Pair<>(0, 0)));
    private Knot followingKnot = null;

    public void move(Movement.Direction direction) {
        positionX += direction.getDiffX();
        positionY += direction.getDiffY();
        saveLocation();
        pullFollowing();
    }

    public void move(int positionX, int positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        saveLocation();
        if (followingKnot != null) {
            pullFollowing();
        }
    }

    private void saveLocation() {
        visitedLocations.add(new Pair<>(positionX, positionY));
    }

    private void pullFollowing() {
        if (Math.abs(positionX - followingKnot.positionX) > 1 && Math.abs(positionY - followingKnot.positionY) > 1) {
            followingKnot.move(followingKnot.getPositionX() + (int) Math.signum(positionX - followingKnot.positionX),
                    followingKnot.getPositionY() + (int) Math.signum(positionY - followingKnot.positionY));
        } else {
            if (Math.abs(positionX - followingKnot.positionX) > 1) {
                followingKnot.move(followingKnot.getPositionX() + (int) Math.signum(positionX - followingKnot.positionX), positionY);
            }
            if (Math.abs(positionY - followingKnot.positionY) > 1) {
                followingKnot.move(positionX, followingKnot.getPositionY() + (int) Math.signum(positionY - followingKnot.positionY));
            }
        }
    }
}
