package adventofcode.day9;

import lombok.Data;
import lombok.Getter;

@Data
public class Movement {
    private final Direction direction;
    private final int amount;

    public Movement(String input) {
        String[] split = input.split(" ");
        switch (split[0]) {
            case "U" -> direction = Direction.Up;
            case "D" -> direction = Direction.Down;
            case "L" -> direction = Direction.Left;
            case "R" -> direction = Direction.Right;
            default -> throw new IllegalStateException("Unexpected value: " + split[0]);
        }
        amount = Integer.parseInt(split[1]);
    }


    public enum Direction {
        Up(1,0),
        Down(-1, 0),
        Right(0, 1),
        Left(0, -1);

        @Getter
        private final int diffX;
        @Getter
        private final int diffY;

        Direction(int diffX, int diffY) {
            this.diffX = diffX;
            this.diffY = diffY;
        }


    }
}
