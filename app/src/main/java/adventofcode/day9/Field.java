package adventofcode.day9;


import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Field {
    private final List<Knot> knots = new ArrayList<>(10);


    public Field(int amountOfKnots) {
        for (int i = 0; i < amountOfKnots; i++) {
            knots.add(new Knot());
            if (i > 0) {
                knots.get(i - 1).setFollowingKnot(knots.get(i));
            }
        }
    }

    public void move(Movement movement) {
        for (int i = 0; i < movement.getAmount(); i++) {
            knots.get(0).move(movement.getDirection());
        }
    }

    public Set<Pair<Integer, Integer>> getTailLocations() {
        return knots.get(knots.size() - 1).getVisitedLocations();
    }
}
