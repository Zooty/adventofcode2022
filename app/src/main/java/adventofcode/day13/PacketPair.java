package adventofcode.day13;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.javatuples.Pair;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class PacketPair {
    private final Pair<Packet, Packet> pair;
    @EqualsAndHashCode.Include
    private final int id;
}
