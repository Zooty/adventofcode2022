package adventofcode.day13;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ListPacket extends Packet {
    private final List<Packet> values = new ArrayList<>();

    public ListPacket(Set<Packet> list) {
        values.addAll(list);
    }

    public ListPacket(String input) {
        if (!"[]".equals(input)) {
            int level = 0;
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < input.length(); i++) {
                if (input.charAt(i) == ',' && level == 0 || level == -1) {
                    values.add(Packet.of(stringBuilder.toString()));
                    stringBuilder.setLength(0);
                } else {
                    stringBuilder.append(input.charAt(i));
                }
                if (input.charAt(i) == '[') level++;
                if (input.charAt(i) == ']') level--;
            }
            stringBuilder.setLength(stringBuilder.length() - 1);
            values.add(Packet.of(stringBuilder.toString()));
        }
    }

    @Override
    public int compareTo(@NotNull Packet other) {
        if (other instanceof ValuePacket otherValue) {
            return compareTo(new ListPacket(Collections.singleton(otherValue)));
        }
        if (other instanceof ListPacket otherList) {
//            if (otherList.values.size() < this.values.size()) return +1;
            int i = 0;
            for (; i < this.values.size(); i++) {
                if (otherList.values.size() < i + 1) {
                    return 1;
                }
                int comparedValue = this.values.get(i).compareTo(otherList.values.get(i));
                if (comparedValue != 0) {
                    return comparedValue;
                }
            }
            return otherList.values.size() == this.values.size() ? 0 : -1;
        }
        throw new IllegalArgumentException("Can't compare with " + other.getClass().getName());
    }
}
