package adventofcode.day13;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;

public class ValuePacket extends Packet {
    @Getter
    private final Integer value;

    public ValuePacket(String input) {
        this.value = Integer.parseInt(input);
    }

    public ValuePacket(Integer value) {
        this.value = value;
    }

    @Override
    public int compareTo(@NotNull Packet other) {
        if (other instanceof ValuePacket otherValue) {
            return this.value.compareTo(otherValue.getValue());
        }
        if (other instanceof ListPacket otherList) {
            return new ListPacket(Collections.singleton(new ValuePacket(this.value))).compareTo(otherList);
        }
        throw new IllegalArgumentException("Can't compare with " + other.getClass().getName());
    }
}
