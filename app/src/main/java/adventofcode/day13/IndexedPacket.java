package adventofcode.day13;

import lombok.Data;

@Data
public class IndexedPacket {
    private final int id;
    private final Packet packet;
}
