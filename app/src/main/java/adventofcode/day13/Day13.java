package adventofcode.day13;

import adventofcode.IDay;
import lombok.Getter;
import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Day13 implements IDay {
    private final List<PacketPair> pairs = new ArrayList<>();
    private final List<IndexedPacket> packets = new ArrayList<>();
    @Getter
    private final int day = 13;

    public Day13() {
        String[] lines = getInputString().split("\n");
        for (int i = 0; i < lines.length; i += 3) {
            pairs.add(new PacketPair(
                    new Pair<>(
                            Packet.of(lines[i]),
                            Packet.of(lines[i + 1])),
                    (i / 3) + 1));
            packets.add(new IndexedPacket(i, Packet.of(lines[i])));
            packets.add(new IndexedPacket(i + 1, Packet.of(lines[i + 1])));
        }
        packets.add(new IndexedPacket(-2, Packet.of("[[2]]")));
        packets.add(new IndexedPacket(-6, Packet.of("[[6]]")));
    }

    @Override
    public String getSolution1() {
        return String.valueOf(pairs.stream()
                .filter(packetPair -> packetPair.getPair().getValue0().compareTo(packetPair.getPair().getValue1()) < 0)
                .mapToInt(PacketPair::getId)
                .sum());
    }

    @Override
    public String getSolution2() {
        packets.sort(Comparator.comparing(IndexedPacket::getPacket));
        int decoder = 1;
        for (int i = 0, packetsSize = packets.size(); i < packetsSize; i++) {
            IndexedPacket packet = packets.get(i);
            if (packet.getId() < 0) {
                decoder *= i + 1;
            }
        }
        return String.valueOf(decoder);
    }
}
