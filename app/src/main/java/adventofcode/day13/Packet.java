package adventofcode.day13;

import lombok.Data;

@Data
public abstract class Packet implements Comparable<Packet> {

    public static Packet of(String value) {
        if (value.startsWith("["))
            return new ListPacket(value);
        return new ValuePacket(value);
    }
}
