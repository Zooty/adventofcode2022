package adventofcode.day6;

import adventofcode.IDay;
import lombok.Getter;

public class Day6 implements IDay {
    @Getter
    final int day = 6;

    @Override
    public String getSolution1() {
        return getMarker(4);
    }

    @Override
    public String getSolution2() {
        return getMarker(14);
    }

    private String getMarker(int length) {
        String inputString = getInputString().trim();
        for (int i = length; i < inputString.length(); i++) {
            if (inputString.substring(i - length, i).chars().distinct().count() == length) {
                return String.valueOf(i);
            }
        }
        throw new IllegalArgumentException("No marker found");
    }
}
