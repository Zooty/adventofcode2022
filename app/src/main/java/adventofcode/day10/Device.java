package adventofcode.day10;

import adventofcode.day10.instruction.AddX;
import adventofcode.day10.instruction.Instruction;
import lombok.Getter;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Device {
    private final List<Instruction> instructionList;
    private int registerX = 1;

    private Instruction runningInstruction;
    private int runningCommandCyclesLeft;
    private int cycle = 0;
    private String renderedString;
    @Getter
    private Map<Integer, Integer> signalAtCycle = new HashMap<>();

    public Device(List<Instruction> instructionList) {
        this.instructionList = instructionList;
    }

    public void run() {
        if (instructionList.isEmpty()) return;
        Iterator<Instruction> instructionIterator = instructionList.iterator();
        loadInstruction(instructionIterator.next());
        StringBuilder stringBuilder = new StringBuilder();
        while (instructionIterator.hasNext() || runningCommandCyclesLeft > 0) {
            if (runningCommandCyclesLeft <= 0) {
                if (runningInstruction != null) {
                    switch (runningInstruction.getType()) {
                        case Noop -> {
                        }
                        case AddX -> registerX += ((AddX) runningInstruction).getValue();
                    }
                }
                loadInstruction(instructionIterator.next());
            }
            runningCommandCyclesLeft--;
            stringBuilder.append(isPixelLit() ? "#" : ".");
            signalAtCycle.put(++cycle, registerX * cycle);
            if (cycle % 40 == 0) stringBuilder.append("\n");
        }
        renderedString = stringBuilder.toString().trim();
    }

    public String render() {
        return renderedString;
    }

    private boolean isPixelLit() {
        int rowPosition = cycle % 40;
        return Math.abs(rowPosition - registerX) < 2;
    }

    private void loadInstruction(Instruction instruction) {
        runningInstruction = instruction;
        runningCommandCyclesLeft = instruction.getCyclesNeeded();
    }
}
