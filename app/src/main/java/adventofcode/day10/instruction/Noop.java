package adventofcode.day10.instruction;

public class Noop extends Instruction{
    public Noop() {
        super(1, InstructionType.Noop);
    }
}
