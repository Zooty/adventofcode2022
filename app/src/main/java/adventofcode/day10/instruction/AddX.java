package adventofcode.day10.instruction;

import lombok.Getter;


public class AddX extends Instruction {
    @Getter
    private final int value;
    public AddX(int value) {
        super(2, InstructionType.AddX);
        this.value = value;
    }
}
