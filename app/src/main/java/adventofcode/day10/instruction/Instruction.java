package adventofcode.day10.instruction;

import lombok.Data;


@Data
public abstract class Instruction {
    private final int cyclesNeeded;
    private final InstructionType type;

    public static Instruction createInstruction(String input) {
        String[] split = input.split(" ");
        switch (split[0].strip()) {
            case "noop" -> {
                return new Noop();
            }
            case "addx" -> {
                return new AddX(Integer.parseInt(split[1].trim()));
            }
            default -> throw new IllegalStateException("Unexpected value: " + split[0]);
        }
    }
}
