package adventofcode.day10.instruction;

public enum InstructionType {
    Noop,
    AddX
}
