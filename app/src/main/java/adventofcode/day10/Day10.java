package adventofcode.day10;

import adventofcode.IDay;
import adventofcode.day10.instruction.Instruction;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Day10 implements IDay {
    @Getter
    private final int day = 10;
    private final List<Instruction> instructionList = new ArrayList<>();

    public Day10() {
        String[] lines = getInputString().split("\n");
        for (String line : lines) {
            instructionList.add(Instruction.createInstruction(line));
        }
    }

    @Override
    public String getSolution1() {
        Device device = new Device(instructionList);
        device.run();
        Map<Integer, Integer> signalStrengths = device.getSignalAtCycle();
        return String.valueOf(List.of(
                        signalStrengths.get(20),
                        signalStrengths.get(60),
                        signalStrengths.get(100),
                        signalStrengths.get(140),
                        signalStrengths.get(180),
                        signalStrengths.get(220))
                .stream().reduce(0, Integer::sum));
    }

    @Override
    public String getSolution2() {
        Device device = new Device(instructionList);
        device.run();
        return "\n" + device.render();
    }
}
